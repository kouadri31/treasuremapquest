package treasureMapQuest.objects;

import org.apache.commons.lang3.StringUtils;
import treasureMapQuest.Utils.ApplicationConstant;
import treasureMapQuest.enums.MapObjectTypeEnum;

public class Treasure extends MapObject {

    private int treasureCount;
    private Map map;

    public Treasure(Map map, int treasureCount, int x, int y, MapObjectTypeEnum objectTypeEnum){
        super(x, y, objectTypeEnum);
        this.map = map;
        this.treasureCount = treasureCount;
    }

    @Override
    protected String getLogo() {
        return "T"+"("+treasureCount+")";
    }

    @Override
    protected void action() {
        this.treasureCount = this.treasureCount - 1;
        if(treasureCount <= 0){
            map.remouve(this.getCoordinateXY().getLeft().intValue(),this.getCoordinateXY().getRight().intValue(),this);
        }
    }

    @Override
    protected String getOutput() {
        StringBuilder sb = new StringBuilder();
        sb.append(MapObjectTypeEnum.TREASURE.getLogo()).append(StringUtils.SPACE).append(ApplicationConstant.DASH)
                .append(StringUtils.SPACE).append(this.getCoordinateXY().getLeft()).append(StringUtils.SPACE)
                .append(ApplicationConstant.DASH).append(StringUtils.SPACE).append(this.getCoordinateXY().getRight())
                .append(ApplicationConstant.DASH).append(StringUtils.SPACE).append(treasureCount);
        return sb.toString();
    }
}
