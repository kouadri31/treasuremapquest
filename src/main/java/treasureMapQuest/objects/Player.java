package treasureMapQuest.objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import treasureMapQuest.Utils.ApplicationConstant;
import treasureMapQuest.enums.ActionsEnum;
import treasureMapQuest.enums.DirectionsEnum;
import treasureMapQuest.enums.MapObjectTypeEnum;
import treasureMapQuest.enums.OrientationEnum;

import java.util.List;
import java.util.Optional;

public class Player extends MapObject {

    private OrientationEnum orientation;
    private int treasureCount;
    private Map map;
    private List<String> movements;
    private String name;

    public Player(Map map, int x, int y, MapObjectTypeEnum objectTypeEnum,OrientationEnum orientation,List<String> movements,String name){
        super(x, y, objectTypeEnum);
        this.map = map;
        this.orientation = orientation;
        this.treasureCount = 0;
        this.movements = movements;
        this.name = name;
    }

    /**
     *
     */
    private void moveForward() throws Exception {
        Pair<Integer,Integer> nextCoordinateXY = this.getNextCoordinate();
        if(this.map.set(nextCoordinateXY.getLeft(),nextCoordinateXY.getRight(),this)){
            this.setCoordinateXY(nextCoordinateXY);
            List<MapObject> mapObject = this.map.get(nextCoordinateXY.getLeft(),nextCoordinateXY.getRight());
            if(mapObject != null){
                Optional<MapObject> objectOptional = mapObject.stream().filter(element -> MapObjectTypeEnum.TREASURE.equals(element.getMapObjectType())).findFirst();
                if(objectOptional.isPresent()){
                    objectOptional.get().action();
                    this.treasureCount = this.treasureCount + 1;
                }
            }
        }
    }

    /**
     *
     */
    private Pair<Integer,Integer> getNextCoordinate() throws Exception {
        switch (orientation) {
            case E -> {
                return Pair.of(this.getCoordinateXY().getLeft() + 1, this.getCoordinateXY().getRight());
            }
            case N -> {
                return Pair.of(this.getCoordinateXY().getLeft(), this.getCoordinateXY().getRight() - 1);
            }
            case O -> {
                return Pair.of(this.getCoordinateXY().getLeft() - 1, this.getCoordinateXY().getRight());
            }
            case S -> {
                return Pair.of(this.getCoordinateXY().getLeft(), this.getCoordinateXY().getRight() + 1);
            }
            default -> {
                throw new Exception("add somthing here");
            }
        }
    }

    /**
     *
     * @param direction
     */
    private void changeOrientation(DirectionsEnum direction){
        switch (direction){
            case G -> {
                this.orientation = OrientationEnum.getByNumber((orientation.getNumber() - 1 + 4) % 4);
            }
            case D -> {
                this.orientation = OrientationEnum.getByNumber((orientation.getNumber() + 1) % 4);
            }

        }
    }

    /**
     *
     */
    public void startPlaying() throws Exception {
        for(String movement : movements ){
            if(ActionsEnum.contains(movement)){
                this.moveForward();
            } else if(DirectionsEnum.contains(movement)){
                this.changeOrientation(DirectionsEnum.valueOf(movement));
            }
        }
    }

    /**
     *
     */
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Player :").append(StringUtils.SPACE).append(name).append(StringUtils.LF);
        sb.append("Orientation :").append(StringUtils.SPACE).append(orientation).append(StringUtils.LF);
        sb.append("Score :").append(StringUtils.SPACE).append(treasureCount).append(StringUtils.LF);
        return sb.toString();
    }

    @Override
    protected String getLogo() {
        return "A"+"("+name+")";
    }

    @Override
    protected void action() {
        // No action is implemented for now. Coming soon :D
    }

    @Override
    protected String getOutput() {
        StringBuilder sb = new StringBuilder();
        sb.append(MapObjectTypeEnum.PLAYER.getLogo()).append(StringUtils.SPACE).append(ApplicationConstant.DASH)
                .append(StringUtils.SPACE).append(this.name).append(StringUtils.SPACE).append(ApplicationConstant.DASH)
                .append(StringUtils.SPACE).append(this.getCoordinateXY().getLeft()).append(StringUtils.SPACE)
                .append(ApplicationConstant.DASH).append(StringUtils.SPACE).append(this.getCoordinateXY().getRight()).append(StringUtils.SPACE).append(ApplicationConstant.DASH)
                .append(StringUtils.SPACE).append(this.orientation).append(StringUtils.SPACE).append(ApplicationConstant.DASH)
                .append(StringUtils.SPACE).append(treasureCount);
        return sb.toString();
    }
}
