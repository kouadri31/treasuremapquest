package treasureMapQuest.objects;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;
import treasureMapQuest.enums.MapObjectTypeEnum;

@Getter
@Setter
public abstract class MapObject {

    private MapObjectTypeEnum mapObjectType;
    private Pair<Integer,Integer> coordinateXY;

    protected MapObject(int x,int y,MapObjectTypeEnum objectTypeEnum){
        this.coordinateXY = Pair.of(x,y);
        this.mapObjectType = objectTypeEnum;
    }

    protected abstract String getLogo();


    protected abstract String getOutput();

    protected abstract void action();

}
