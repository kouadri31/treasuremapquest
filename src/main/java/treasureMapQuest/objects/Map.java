package treasureMapQuest.objects;

import org.apache.commons.lang3.StringUtils;
import treasureMapQuest.Utils.ApplicationConstant;
import treasureMapQuest.enums.MapObjectTypeEnum;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Map {

    private final java.util.Map<String, List<MapObject>> matrix;
    private final int columns;
    private final int rows;
    private static final String COMA =",";

    public Map(int columns, int rows) {
        this.columns = columns;
        this.rows = rows;
        this.matrix = new HashMap<>();
    }

    /**
     *
     * @param x
     * @param y
     * @param value
     */
    public boolean set(int x,int y,MapObject value) {
        if (isValidCoordinate(x, y) && canAddObject(x, y, value)) {
            List<MapObject> cell = get(x,y);
            if(cell == null){
                cell = new ArrayList<>();
            }
            if(MapObjectTypeEnum.PLAYER.equals(value.getMapObjectType())){
                List<MapObject> objets = matrix.get(getKey(value.getCoordinateXY().getLeft(),value.getCoordinateXY().getRight()));
                if(objets != null && objets.contains(value)){
                    objets.remove(value);
                }
                if(objets.isEmpty()){
                    matrix.remove(getKey(value.getCoordinateXY().getLeft(),value.getCoordinateXY().getRight()));
                }
            }
            cell.add(value);
            matrix.put(getKey(x,y), cell);
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     */
    public boolean addObjectToTheMap(MapObject mapObject){
        int x,y;
        x = mapObject.getCoordinateXY().getLeft();
        y = mapObject.getCoordinateXY().getRight();
        if (isValidCoordinate(x, y) && canAddObject(x, y, mapObject)) {
            List<MapObject> cell = new ArrayList<>();
            cell.add(mapObject);
            matrix.put(getKey(x,y), cell);
            return true;
        }
        return false;
    }

    // Method to retrieve the value of a specific cell in the map

    /**
     *
     * @param x
     * @param y
     * @return
     */
    public List<MapObject> get(int x, int y) {
        if (isValidCoordinate(x, y)) {
            return matrix.get(getKey(x,y));
        } else {
            System.out.println("Invalid coordinates. ("+x+","+y+")");
            return null;
        }
    }

    /**
     *
     * @param x
     * @param y
     * @return
     */
    public void remouve(int x, int y, MapObject object) {
        if (isValidCoordinate(x, y)) {
            List<MapObject> objects = matrix.get(getKey(x,y));
            if(objects != null){
                objects.remove(object);
                if(objects.isEmpty()){
                    matrix.remove(getKey(x,y));
                }
            }
        } else {
            System.out.println("Invalid coordinates.");
        }
    }

    // Method to retrieve the value of a specific cell in the map

    /**
     *
     * @param x
     * @param y
     * @return
     */
    private String getKey(int x, int y) {
        return x + COMA + y;
    }

    // Method to check if given coordinates are within the map boundaries

    /**
     *
     * @param x
     * @param y
     * @return
     */
    private boolean isValidCoordinate(int x, int y) {
        return x >= 0 && x < columns && y >= 0 && y < rows;
    }

    // Method to check if we can add object in this coordinates or not

    /**
     *
     * @param x
     * @param y
     * @param object
     * @return
     */
    private boolean canAddObject(int x, int y,MapObject object) {
        List<MapObject> existantObject = get(x,y);
        // If we want to put a player and treasure in the same coordinates we can
        if((existantObject != null && existantObject.size() < 2 && MapObjectTypeEnum.TREASURE.equals(existantObject.get(0).getMapObjectType()) && MapObjectTypeEnum.PLAYER.equals(object.getMapObjectType())) || existantObject == null){
            return true;
        }
        return false;
    }

    /**
     *
     */
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(int y = 0; rows > y;y++){
            for(int x = 0; columns > x;x++){
                if(matrix.containsKey(getKey(x,y))){
                    for(MapObject object : matrix.get(getKey(x,y))){
                        sb.append(object.getLogo());
                        if(MapObjectTypeEnum.TREASURE.equals(object.getMapObjectType())){
                            sb.append("\t").append("\t");
                        }else if(MapObjectTypeEnum.PLAYER.equals(object.getMapObjectType())){
                            sb.append("\t").append("\t");

                        }else if(MapObjectTypeEnum.MOUNTAIN.equals(object.getMapObjectType())){
                            sb.append("\t").append("\t").append("\t");

                        }
                    }
                }else{
                    sb.append(".").append("\t").append("\t").append("\t");
                }
            }
            sb.append(StringUtils.LF);
        }

        return sb.toString();
    }

    /**
     *
     */
    public String getOutput(){
        StringBuilder sb = new StringBuilder();
        sb.append("M").append(StringUtils.SPACE).append(ApplicationConstant.DASH).append(StringUtils.SPACE)
                .append(columns).append(StringUtils.SPACE).append(ApplicationConstant.DASH).append(StringUtils.SPACE).append(rows).append(StringUtils.LF);
        for (List<MapObject> value : matrix.values()) {
            for(MapObject object : value){
                sb.append(object.getOutput()).append(StringUtils.LF);
            }
        }
        return sb.toString();
    }

}
