package treasureMapQuest.objects;

import org.apache.commons.lang3.StringUtils;
import treasureMapQuest.Utils.ApplicationConstant;
import treasureMapQuest.enums.MapObjectTypeEnum;

public class Mountain extends MapObject{

    public Mountain(int x, int y, MapObjectTypeEnum objectTypeEnum){
        super(x, y, objectTypeEnum);
    }

    @Override
    protected String getLogo() {
        return "M";
    }

    @Override
    protected String getOutput() {
        StringBuilder sb = new StringBuilder();
        sb.append(MapObjectTypeEnum.MOUNTAIN.getLogo()).append(StringUtils.SPACE).append(ApplicationConstant.DASH)
                .append(StringUtils.SPACE).append(this.getCoordinateXY().getLeft()).append(StringUtils.SPACE)
                .append(ApplicationConstant.DASH).append(StringUtils.SPACE).append(this.getCoordinateXY().getRight());
        return sb.toString();
    }

    @Override
    protected void action() {
        // No action is implemented for now. Coming soon :D
    }
}
