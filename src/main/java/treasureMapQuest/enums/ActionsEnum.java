package treasureMapQuest.enums;

public enum ActionsEnum {

    A("Avancer");

    private String info;

    ActionsEnum(String info){
        this.info = info;
    }

    public static boolean contains(String name) {
        for (ActionsEnum action : ActionsEnum.values()) {
            if (action.name().equals(name)) {
                return true;
            }
        }
        return false;
    }
}
