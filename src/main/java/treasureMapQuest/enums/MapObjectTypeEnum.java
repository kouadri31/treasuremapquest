package treasureMapQuest.enums;

import lombok.Getter;

@Getter
public enum MapObjectTypeEnum {
    MOUNTAIN("This is a mountain.","M", Boolean.FALSE),
    PLAYER("This is a player.","A", Boolean.FALSE),
    TREASURE("This is a treasure.","T", Boolean.TRUE);

    private final String info;
    private final String logo;
    private final Boolean allowOverlap;

    MapObjectTypeEnum(String info, String logo, Boolean allowOverlap) {
        this.info = info;
        this.logo = logo;
        this.allowOverlap = allowOverlap;
    }

}
