package treasureMapQuest.enums;

public enum DirectionsEnum {

    G("Gauche"),D("Droite");

    private String info;

    DirectionsEnum(String info){
        this.info = info;
    }


    public static boolean contains(String name) {
        for (DirectionsEnum direction : DirectionsEnum.values()) {
            if (direction.name().equals(name)) {
                return true;
            }
        }
        return false;
    }
}
