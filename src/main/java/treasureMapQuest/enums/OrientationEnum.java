package treasureMapQuest.enums;

import lombok.Getter;

@Getter
public enum OrientationEnum {
    N("Nord",0),E("Est",1),S("Sud",2),O("Ouest",3);

    private String info;

    private int number;

    OrientationEnum(String info,int number){
        this.info = info;
        this.number = number;
    }


    public static OrientationEnum getByNumber(int number) {
        for (OrientationEnum orientation : OrientationEnum.values()) {
            if (orientation.number == number) {
                return orientation;
            }
        }
        throw new IllegalArgumentException("Invalid number: " + number);
    }

}
