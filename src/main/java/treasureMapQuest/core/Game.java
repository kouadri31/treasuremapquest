package treasureMapQuest.core;

import lombok.AllArgsConstructor;
import org.apache.commons.io.FileUtils;
import treasureMapQuest.enums.MapObjectTypeEnum;
import treasureMapQuest.enums.OrientationEnum;
import treasureMapQuest.objects.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@AllArgsConstructor
public class Game {

    private String filePath;
    private static final String MAP_REGEX = "^C - (\\d+) - (\\d+)$";
    private static final String MOUNTAIN_REGEX = "^M - (\\d+) - (\\d+)$";
    private static final String TREASURE_REGEX = "^T - (\\d+) - (\\d+) - (\\d+)$";
    private static final String PLAYER_REGEX = "^A - ([A-Za-z]+) - (\\d+) - (\\d+) - ([NSEO]) - ([ADG]+)$";
    private static final String COMMENT = "^#.*";

    public void startTheGame() throws Exception {
        File file = new File(filePath);
        if(file.exists()){
            String content = FileUtils.readFileToString(file, "UTF-8");
            List<String> lines = List.of(content.split("\\r?\\n"));
            Map map = null;
            for (String line : lines){
                if(line.matches(MAP_REGEX)){
                    map = getMapFromLine(line);
                    break;
                }
            }

            if(map != null){
                for (String line : lines){
                    if(line.matches(MOUNTAIN_REGEX)){
                        if(!map.addObjectToTheMap(getMountainFromLine(line))){
                            throw new Exception();
                        }
                    }
                }

                for (String line : lines){
                    if(line.matches(TREASURE_REGEX)){
                        if(!map.addObjectToTheMap(getTreasureFromLine(line,map))){
                            throw new Exception();
                        }
                    }
                }
                List<Player> players = new ArrayList<>();
                for (String line : lines){
                    if(line.matches(PLAYER_REGEX)){
                        Player player = getPlayerFromLine(line,map);
                        if(!map.addObjectToTheMap(player)){
                            throw new Exception();
                        }
                        players.add(player);
                    }
                }

                for(Player player : players){
                    player.startPlaying();
                }

                System.out.println(map.getOutput());
                System.out.println(map.toString());

            }else{
                // throw exception
            }

        }
    }

    /**
     *
     * @param line
     * @return
     * @throws IOException
     */
    private Map getMapFromLine(String line) throws IOException {
        Pattern pattern = Pattern.compile(MAP_REGEX);
        Matcher matcher = pattern.matcher(line);
        if (matcher.matches()) {
            int x = Integer.parseInt(matcher.group(1));
            int y = Integer.parseInt(matcher.group(2));
            return new Map(x,y);
        }
        // throw exception
        return null;
    }

    private Mountain getMountainFromLine(String line) throws IOException {
        Pattern pattern = Pattern.compile(MOUNTAIN_REGEX);
        Matcher matcher = pattern.matcher(line);
        if (matcher.matches()) {
            int x = Integer.parseInt(matcher.group(1));
            int y = Integer.parseInt(matcher.group(2));
            return new Mountain(x,y, MapObjectTypeEnum.MOUNTAIN);
        }
        // throw exception
        return null;
    }

    private Treasure getTreasureFromLine(String line,Map map) throws IOException {
        Pattern pattern = Pattern.compile(TREASURE_REGEX);
        Matcher matcher = pattern.matcher(line);
        if (matcher.matches()) {
            int x = Integer.parseInt(matcher.group(1));
            int y = Integer.parseInt(matcher.group(2));
            int treasureCount = Integer.parseInt(matcher.group(3));
            return new Treasure(map,treasureCount,x,y, MapObjectTypeEnum.TREASURE);
        }
        // throw exception
        return null;
    }

    private Player getPlayerFromLine(String line, Map map) throws IOException {
        Pattern pattern = Pattern.compile(PLAYER_REGEX);
        Matcher matcher = pattern.matcher(line);
        if (matcher.matches()) {
            String name = matcher.group(1);
            int x = Integer.parseInt(matcher.group(2));
            int y = Integer.parseInt(matcher.group(3));
            String orientation = matcher.group(4);
            List<String> movements = matcher.group(5).chars().mapToObj(c -> String.valueOf((char) c)).collect(Collectors.toList());
            return new Player(map,x,y, MapObjectTypeEnum.PLAYER, OrientationEnum.valueOf(orientation),movements,name);
        }
        // throw exception
        return null;
    }


}
