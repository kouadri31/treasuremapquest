package treasureMapQuest;

import treasureMapQuest.core.Game;

public class TreasureMapQuest {

    public static void main(String[] args) throws Exception {

        if(args != null && args.length != 0){
            Game game = new Game(args[0]);
            game.startTheGame();
        }
    }
}
